﻿CREATE TABLE [staging].[Skill] (
    [Key]              INT           NOT NULL,
    [Name]             NVARCHAR (64) NULL,
    [Revision]         INT           NOT NULL,
    [Description]      NVARCHAR (64) NULL,
    [Created_Date]     DATETIME      NULL,
    [Created_By]       NVARCHAR (64) NULL,
    [Ebinary_Checksum] VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC, [Revision] ASC)
);


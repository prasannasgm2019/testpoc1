﻿CREATE TABLE [staging].[Country] (
    [Key]              INT           NOT NULL,
    [Revision]         INT           NOT NULL,
    [Name]             NVARCHAR (64) NULL,
    [CountryID_Key]    NVARCHAR (64) NULL,
    [Created_Date]     DATETIME      NULL,
    [Created_By]       NVARCHAR (64) NULL,
    [Ebinary_Checksum] VARCHAR (MAX) NULL,
	test varchar(30)
    PRIMARY KEY CLUSTERED ([Key] ASC, [Revision] ASC)
);


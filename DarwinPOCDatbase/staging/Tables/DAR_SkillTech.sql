﻿CREATE TABLE [staging].[DAR_SkillTech] (
    [Key]              BIGINT         NULL,
    [Revision]         BIGINT         NULL,
    [Name]             NVARCHAR (MAX) NULL,
    [Description]      NVARCHAR (MAX) NULL,
    [Type]             NVARCHAR (MAX) NULL,
    [CreatedDate]      DATETIME       NULL,
    [CreatedBy]        NVARCHAR (MAX) NULL,
    [Ebinary_Checksum] NVARCHAR (MAX) NULL
);


﻿CREATE TABLE [staging].[DAR_SkillTier_SkillTech_Map] (
    [DAR_SkillTier_Key]      INT           NOT NULL,
    [DAR_SkillTier_Revision] INT           NOT NULL,
    [DAR_SkillTech_Key]      INT           NOT NULL,
    [Created_Date]           DATETIME      NULL,
    [Created_By]             NVARCHAR (64) NULL
);


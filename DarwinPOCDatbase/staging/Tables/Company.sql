﻿CREATE TABLE [staging].[Company] (
    [Key]              INT           NOT NULL,
    [Revision]         INT           NOT NULL,
    [Name]             NVARCHAR (64) NULL,
    [Created_Date]     DATETIME      NULL,
    [Created_By]       NVARCHAR (64) NULL,
    [Ebinary_Checksum] VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC, [Revision] ASC)
);


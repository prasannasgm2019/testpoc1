﻿CREATE TABLE [staging].[DAR_SkillTier] (
    [Key]              INT           NOT NULL,
    [Revision]         INT           NOT NULL,
    [Name]             NVARCHAR (64) NULL,
    [Description]      NVARCHAR (64) NULL,
    [Type]             NVARCHAR (64) NULL,
    [SkillTech1]       NVARCHAR (64) NULL,
    [Created_Date]     DATETIME      NULL,
    [Created_By]       NVARCHAR (64) NULL,
    [Ebinary_Checksum] VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC, [Revision] ASC)
);


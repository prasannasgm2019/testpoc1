﻿CREATE TABLE [staging].[DAR_ReportingSupervisor] (
    [Key]                     INT           NOT NULL,
    [Revision]                INT           NOT NULL,
    [Name]                    NVARCHAR (64) NULL,
    [ID]                      NVARCHAR (64) NULL,
    [Area_Key]                INT           NULL,
    [EmployeeNumber]          NVARCHAR (64) NULL,
    [ReportingSupervisorName] NVARCHAR (64) NULL,
    [ReportingSupervisorID]   NVARCHAR (64) NULL,
    [Crnt_Flg]                CHAR (1)      NULL,
    [Created_By]              VARCHAR (64)  NULL,
    [Modified_By]             VARCHAR (64)  NULL,
    [Ebinary_Checksum]        VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC, [Revision] ASC)
);


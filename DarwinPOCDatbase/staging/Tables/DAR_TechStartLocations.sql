﻿CREATE TABLE [staging].[DAR_TechStartLocations] (
    [Key]                INT           NOT NULL,
    [Revision]           INT           NOT NULL,
    [Name]               NVARCHAR (64) NULL,
    [Street]             NVARCHAR (64) NULL,
    [City]               NVARCHAR (64) NULL,
    [State]              NVARCHAR (64) NULL,
    [Latitude]           INT           NOT NULL,
    [Longitude]          INT           NOT NULL,
    [ParentDistrict_Key] INT           NOT NULL,
    [Created_Date]       DATETIME      NULL,
    [Created_By]         NVARCHAR (64) NULL,
    [Ebinary_Checksum]   VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC, [Revision] ASC)
);


﻿CREATE TABLE [staging].[District] (
    [Key]                    INT           NOT NULL,
    [Name]                   NVARCHAR (64) NULL,
    [Revision]               INT           NOT NULL,
    [RegionParent_Key]       INT           NULL,
    [TimeZone]               NVARCHAR (64) NULL,
    [SameSiteTimeRadius]     INT           NULL,
    [SameSiteDistanceRadius] INT           NULL,
    [City]                   NVARCHAR (64) NULL,
    [Postcode]               NVARCHAR (64) NULL,
    [Street]                 NVARCHAR (64) NULL,
    [State]                  NVARCHAR (64) NULL,
    [Longitude]              INT           NULL,
    [Latitude]               INT           NULL,
    [GISDataSource]          INT           NULL,
    [CountryID_Key]          INT           NULL,
    [ParkingTime_SO]         INT           NULL,
    [UTCOffset_SO]           INT           NULL,
    [AverageTravelTime_SO]   INT           NULL,
    [AverageTravelTime]      INT           NULL,
    [Created_Date]           DATETIME      NULL,
    [Created_By]             NVARCHAR (64) NULL,
    [Ebinary_Checksum]       VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC, [Revision] ASC)
);


﻿CREATE TABLE [dbo].[test_jenkin] (
    [name]         VARCHAR (20) NULL,
	[jenkinstest] VARCHAR(40) NULL,
	[jenkinstest1] VARCHAR(40) NULL,
    [createuser]   VARCHAR (20) DEFAULT (user_name()) NULL,
    [createdate]   DATETIME     DEFAULT (getdate()) NULL,
    [modifieduser] VARCHAR (20) NULL,
    [modifieddate] DATETIME     NULL
);


﻿CREATE TABLE [dbo].[test] (
    [name]         VARCHAR (20) NULL,
	[name_demo1]         VARCHAR (20) NULL,
	[name_demo2]         VARCHAR (20) NULL,
	[createuser]   VARCHAR (20) DEFAULT (user_name()) NULL,
    [createdate]   DATETIME     DEFAULT (getdate()) NULL,
    [modifieduser] VARCHAR (20) NULL,
    [modifieddate] DATETIME     NULL
);

